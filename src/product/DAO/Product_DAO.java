package product.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import product.DTO.Product_DTO;

public class Product_DAO {
	
	public Connection getConnection() throws Exception{		
		Connection con = null;
		Class.forName("org.postgresql.Driver");
		String url = "jdbc:postgresql://localhost:5432/java-jdbc";
		String user = "postgres";
		String password  = "root321"; 		
		con = DriverManager.getConnection(url, user, password);
		return con;
	}
	
	public void showProduct() throws Exception{
		Connection con = getConnection();
		String sql = "select * from table_product";
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		System.out.println("ID\tName\t\tQTY\tPrice");
		while(rs.next()){
			System.out.println(rs.getInt(1)+"\t"+rs.getString(2)+"\t\t"+rs.getInt(3)+"\t$"+rs.getFloat(4));
		}
	}
	
	public boolean insertProduct(Product_DTO p) throws Exception{
		boolean b=false;
		Connection con = getConnection();
		String sql = "insert into table_product values(?,?,?,?)";
		PreparedStatement pstmt;
		pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, p.getId());
		pstmt.setString(2, p.getName());
		pstmt.setInt(3, p.getQty());
		pstmt.setFloat(4, p.getPrice());
		int i = pstmt.executeUpdate();
		if(i == 1)
			b = true;
		return b;
	}
	
	public boolean updateProduct(Product_DTO p) throws Exception{
		boolean b=false;
		Connection con = getConnection();
		String sql = "Update table_product set product_name=?, product_qty=?, product_price=? where product_id=?";
		PreparedStatement pstmt;
		pstmt = con.prepareStatement(sql);
		pstmt.setInt(4, p.getId());
		pstmt.setString(1, p.getName());
		pstmt.setInt(2, p.getQty());
		pstmt.setFloat(3, p.getPrice());
		int i = pstmt.executeUpdate();
		if(i == 1)
			b = true;
		return b;
	}
	
	public boolean deleteProduct(int id) throws Exception{
		boolean b=false;
		Connection con = getConnection();
		String sql = "Delete from table_product where product_id=?";
		PreparedStatement pstmt;
		pstmt = con.prepareStatement(sql);
		pstmt.setInt(1, id);	
		int i = pstmt.executeUpdate();
		if(i == 1)
			b = true;
		return b;
	}
	
}
