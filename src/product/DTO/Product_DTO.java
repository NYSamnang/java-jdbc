package product.DTO;

public class Product_DTO {

	private int id;
	private String name;
	private int qty;
	private float price;
	
	
	public Product_DTO(int id, String name, int qty, float price){
		super();
		this.id = id;
		this.name = name;
		this.qty = qty;
		this.price = price;
	}
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public int getQty(){
		return qty;
	}
	public void setQty(int qty){
		this.qty = qty;
	}
	
	public float getPrice(){
		return price;
	}
	public void setPrice(float price){
		this.price = price;
	}
	
}
