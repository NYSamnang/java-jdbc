package product;

import java.util.Scanner;

import product.DAO.Product_DAO;
import product.DTO.Product_DTO;

public class Run {
		public static int id;
		public static int num;
		public static String name;
		public static int qty;
		public static float price;
		public static Product_DAO runDAO = new Product_DAO();
		public static Scanner reader = new Scanner(System.in);
		public static Scanner readID = new Scanner(System.in);
		public static Scanner readMenu = new Scanner(System.in);
	
	public static void main(String[] args) throws Exception{
		
		runDAO.showProduct();
		menu();
			
	}
	
	public static void menu() throws Exception{
		
		System.out.println("\n\tMenu");
		System.out.println("- Enter 1 to Insert Data");
		System.out.println("- Enter 2 to Update Data");
		System.out.println("- Enter 3 to Delete Data");
		System.out.println("- Enter 4 to Exit Program");
		System.out.print(">> ");
		num = readMenu.nextInt();
		switch(num){
		case 1: insertData();
		        break;	
		case 2: updateData();
		        break;
		case 3: deleteData();
				break;
		case 4: 
				System.out.print("Good Bye...");
		        System.exit(0);
		default:
				System.out.println("Invalid number!");
				break;
		}	
	}
	
	public static void insertData() throws Exception{
		
		System.out.println("\nPlease Enter Product Detail to Insert!");
		
		System.out.print("ID: ");
		id = readID.nextInt();
		
		System.out.print("Name: ");
		name = reader.nextLine();
		
		System.out.print("QTY: ");
		qty = reader.nextInt();
		
		System.out.print("Price: ");
		price = reader.nextFloat();
				
		Product_DTO runDTO = new Product_DTO(id, name, qty, price);
		boolean b = runDAO.insertProduct(runDTO);
		if(b==true){
			System.out.println("Insert Success...\n");
		}
		
		runDAO.showProduct();
	}
	
	public static void updateData() throws Exception{
	
		System.out.println("\nPlease Enter Product Detail and ID of Product to Update!");
		
		System.out.print("ID: ");
		id = readID.nextInt();
		
		System.out.print("Name: ");
		name = reader.nextLine();
		
		System.out.print("QTY: ");
		qty = reader.nextInt();
		
		System.out.print("Price: ");
		price = reader.nextFloat();
	
		Product_DTO runDTO = new Product_DTO(id, name, qty, price);
		boolean b = runDAO.updateProduct(runDTO);
		if(b==true){
			System.out.println("Update Success...\n");
		}
		
		runDAO.showProduct();
	}
	
	public static void deleteData() throws Exception{
		
		System.out.println("\nPlease Enter ID of Product to Delete!");

		System.out.print("ID: ");
		id = readID.nextInt();
		
		boolean b = runDAO.deleteProduct(id);
		if(b==true){
			System.out.println("Delete Success...\n");
		}
		
		runDAO.showProduct();
	}

}
